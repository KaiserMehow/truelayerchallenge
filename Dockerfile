FROM rust:1.52

WORKDIR /usr/src/truelayer_challenge
COPY . .

EXPOSE 80

RUN cargo install --path .

CMD ["truelayer_challenge"]

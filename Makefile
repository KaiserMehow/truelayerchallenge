.PHONY: ci
ci:
	cargo test && \
	cargo clippy && \
	cargo fmt --all -- --check

.PHONY: start
start:
	cargo run

.PHONY: start
docker-build:
	docker build -t truelayer-challenge .

.PHONY: docker-run
docker-run:
	docker run -p 8080:80 -it --rm --name truelayer-challenge truelayer-challenge

.PHONY: docker-stop
docker-stop:
	docker stop truelayer-challenge

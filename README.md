# Truelayer challenge

## Dependencies

* docker
* make
* [rustup](https://rustup.rs/)

## Running the server

With docker:

```bash
# If you get an error like "Got permission denied while trying to connect to the Docker daemon socket..." then run the docker commands with sudo, or add your user to the docker group
make docker-build
make docker-run
make docker-stop
```

Start server:

```bash
# to change the port used:
# export PORT=8080
make run
```

Use the endpoints

```bash
# with docker, default port is 8080
# without docker, default port is 80
$ curl localhost:8080/pokemon/mewtwo
{"name":"mewtwo","description":"It was created by a scientist after years of horrific gene splicing and DNA engineering experiments.","habitat":"rare","is_legendary":true}
$ curl localhost:8080/pokemon/translated/mewtwo
{"name":"mewtwo","description":"Created by a scientist after years of horrific gene splicing and dna engineering experiments,  it was.","habitat":"rare","is_legendary":true}
```

## Development

```bash
cargo build
cargo run
cargo fmt
cargo clippy
```

Test:

```bash
make ci
```

## Improvements

* Create a docker image for development and testing
  * that has a volume to the current dir, so we can rebuild inside the container
  * and runs `make ci` by default
* Use a mocking library to help clean up `TestClient`
  * `mockall` looks good, but `httpmock` would let us catch http troubles
    * like forgetting to encode the description (caught by the end-to-end test)
* Better caching
  * Try and reduce how often we need to block a response on external API calls
  * We should have a cache thread running to pre-fill and refresh pokemon entries
    * respecting the cache headers sent by the API
    * There are only about 800 pokemon - we can handle caching them all easily
      * especially if we cache the must smaller `crate::Pokemon` instead of `crate::poke_api::Pokemon`
    * we should also refresh them as they expire
  * For the translations, we need to be more careful
    * we're only allowed 5 hits per hour
      * If we aggressively pre-fill the cache, the API will rate limit use when we need it for user requests
    * but we could keep track of how many times we've hit the API in the last hour
      * If we reach 55 minutes (for example) since we last called the API, then pre-fill 5 random pokemon translations
        * i.e. spend spare calls we don't need for user queries
  * In production - this cache would be backed by a persistent key-value store
    * so that we don't need to re-build it for every instance/on every restart
* Logging
  * Something like sentry to report errors and prometheus to measure API performance (ours and the external APIs)
* Do a better job of normalizing whitespace in the descriptions
  * Use a std or crate function for identifying whitespace characters

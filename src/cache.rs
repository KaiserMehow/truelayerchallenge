use async_std::sync::RwLock;
use std::collections::HashMap;

use std::hash::Hash;

pub struct Cache<K, V> {
    cache: RwLock<HashMap<K, V>>,
}

impl<K, V> Cache<K, V> {
    pub fn new() -> Cache<K, V> {
        Cache {
            cache: RwLock::new(HashMap::new()),
        }
    }

    pub async fn check(&self, name: &K) -> Option<V>
    where
        K: Hash + Eq,
        V: Clone,
    {
        let cache = self.cache.read().await;
        cache.get(name).map(|p| (*p).clone())
    }

    pub async fn update(&self, name: K, pokemon: &V)
    where
        K: Hash + Eq,
        V: Clone,
    {
        self.cache.write().await.insert(name, pokemon.clone());
    }
}

use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::env;
use tide::Request;

use std::{error::Error, fmt};

mod cache;
mod poke_api;
mod translate_api;

use translate_api::translate;

/// Represents the Pokemon returned by our API
/// to the client
/// Derived from the Pokeapi pokemon definitions
#[derive(Serialize, Deserialize)]
struct Pokemon {
    name: String,
    /// this may be translated into a text not present in the original pokeapi pokemon
    /// Should only be None if no english descriptions can be found
    description: Option<String>,
    habitat: Option<String>,
    is_legendary: bool,
}

impl From<poke_api::Pokemon> for Pokemon {
    fn from(pokemon: poke_api::Pokemon) -> Self {
        let mut description = None;

        for flavor_text_entry in pokemon.flavor_text_entries {
            if flavor_text_entry.language.name == "en" {
                description = Some(
                    flavor_text_entry
                        .flavor_text
                        .replace(&['\n', '\u{c}'][..], " "),
                );
                break;
            }
        }

        Pokemon {
            name: pokemon.name,
            description,
            habitat: pokemon.habitat.map(|h| h.name),
            is_legendary: pokemon.is_legendary,
        }
    }
}

#[derive(Debug)]
enum ConfigurationErrors {
    BadPort,
}

impl Error for ConfigurationErrors {}

impl fmt::Display for ConfigurationErrors {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            ConfigurationErrors::BadPort => write!(
                f,
                "PORT environment variable must be between 1 and 65535 (inclusive)"
            ),
        }
    }
}

fn port() -> Result<String, ConfigurationErrors> {
    let port = match env::var("PORT") {
        Ok(port) => port,
        Err(_) => "80".to_string(),
    };
    match port.parse::<u16>() {
        Ok(n) => match n {
            0 => Err(ConfigurationErrors::BadPort),
            _ => Ok(port),
        },
        _ => Err(ConfigurationErrors::BadPort),
    }
}

#[async_trait]
pub trait Get {
    async fn get(url: &str) -> Result<http::Response<isahc::AsyncBody>, isahc::Error>;
}

#[derive(Clone)]
struct HttpClient {}

#[async_trait]
impl Get for HttpClient {
    async fn get(url: &str) -> Result<http::Response<isahc::AsyncBody>, isahc::Error> {
        isahc::get_async(url).await
    }
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    let app = make_server::<HttpClient>();
    let port = port()?;
    app.listen(format!("0.0.0.0:{}", port)).await?;
    Ok(())
}

fn make_server<G: 'static + Get + Send + Sync>() -> tide::Server<()> {
    let mut app = tide::new();

    app.at("/pokemon/translated/:name")
        .get(pokemon_translated_endpoint::<G>);
    app.at("/pokemon/:name").get(pokemon_endpoint::<G>);
    app
}

async fn name_to_pokemon<G: Get>(name: &str) -> Result<Pokemon, tide::Result> {
    match poke_api::lookup_pokemon::<G>(&name).await {
        Ok(api_pokemon) => Ok(api_pokemon.into()),
        Err(e) => {
            return match e {
                poke_api::Errors::NotFound => {
                    Err(Ok(tide::Response::new(tide::StatusCode::NotFound)))
                }
                _ => Err(Ok(tide::Response::new(
                    tide::StatusCode::InternalServerError,
                ))),
            }
        }
    }
}

fn pokemon_response(pokemon: &Pokemon) -> tide::Result {
    let mut res = tide::Response::new(tide::StatusCode::Ok);
    let body = if let Ok(b) = tide::Body::from_json(&pokemon) {
        b
    } else {
        return Ok(tide::Response::new(tide::StatusCode::InternalServerError));
    };
    res.set_body(body);
    Ok(res)
}

async fn pokemon_endpoint<G: Get>(request: Request<()>) -> tide::Result {
    let name = if let Ok(name) = request.param("name") {
        name
    } else {
        // as this is called on a route using :name in the path
        // this should never happen in production
        // and if it does, something else is wrong
        let res = tide::Response::new(tide::StatusCode::InternalServerError);
        return Ok(res);
    };
    let pokemon: Pokemon = match name_to_pokemon::<G>(name).await {
        Ok(p) => p,
        Err(response) => return response,
    };

    pokemon_response(&pokemon)
}

async fn pokemon_translated_endpoint<G: Get>(request: Request<()>) -> tide::Result {
    let name = if let Ok(name) = request.param("name") {
        name
    } else {
        // as this is called on a route using :name in the path
        // this should never happen in production
        // and if it does, something else is wrong
        let res = tide::Response::new(tide::StatusCode::InternalServerError);
        return Ok(res);
    };
    let mut pokemon: Pokemon = match name_to_pokemon::<G>(name).await {
        Ok(p) => p,
        Err(response) => return response,
    };

    if let Some(description) = pokemon.description.as_ref() {
        let translated = if pokemon.habitat == Some("cave".into()) || pokemon.is_legendary {
            translate::<G>(&description, "yoda").await
        } else {
            translate::<G>(&description, "shakespeare").await
        };
        if translated.is_ok() {
            pokemon.description = translated.ok();
        }
    }

    pokemon_response(&pokemon)
}

#[cfg(test)]
mod test {

    use super::*;
    use async_std::fs::File;
    use isahc::prelude::*;
    use tide::http::{Method, Url};

    #[cfg(test)]
    mod from_api_pokemon {

        use super::*;
        use once_cell::sync::Lazy;

        const TEMPLATE_API_POKEMON: Lazy<poke_api::Pokemon> = Lazy::new(|| poke_api::Pokemon {
            name: "mewtwo".to_string(),
            flavor_text_entries: vec![
                poke_api::FlavorText {
                    flavor_text: "text1".to_string(),
                    language: poke_api::Language {
                        name: "en".to_string(),
                    },
                },
                poke_api::FlavorText {
                    flavor_text: "text2".to_string(),
                    language: poke_api::Language {
                        name: "en".to_string(),
                    },
                },
            ],
            is_legendary: true,
            habitat: Some(poke_api::Habitat {
                name: "rare".to_string(),
            }),
        });

        #[test]
        fn test_standard() {
            let api_pokemon = TEMPLATE_API_POKEMON.clone();
            let pokemon: Pokemon = api_pokemon.clone().into();
            assert_eq!(pokemon.name, api_pokemon.name);
            assert_eq!(pokemon.habitat, Some(api_pokemon.habitat.unwrap().name));
            assert_eq!(pokemon.is_legendary, api_pokemon.is_legendary);
            assert_eq!(
                pokemon.description.unwrap(),
                api_pokemon.flavor_text_entries[0].flavor_text
            );
        }

        #[test]
        fn test_no_habitat() {
            let mut api_pokemon = TEMPLATE_API_POKEMON.clone();
            api_pokemon.habitat = None;
            let pokemon: Pokemon = api_pokemon.into();
            assert_eq!(pokemon.habitat, None);
        }

        #[test]
        fn test_not_english_first_flavor_text() {
            let mut api_pokemon = TEMPLATE_API_POKEMON.clone();
            api_pokemon.flavor_text_entries[0].language.name = "fr".to_string();
            let pokemon: Pokemon = api_pokemon.clone().into();
            assert_eq!(
                pokemon.description.unwrap(),
                api_pokemon.flavor_text_entries[1].flavor_text
            );
        }

        #[test]
        fn test_no_english_description() {
            let mut api_pokemon = TEMPLATE_API_POKEMON.clone();
            api_pokemon.flavor_text_entries[0].language.name = "fr".to_string();
            api_pokemon.flavor_text_entries[1].language.name = "fr".to_string();
            let pokemon: Pokemon = api_pokemon.clone().into();
            assert_eq!(pokemon.description, None);
        }

        #[test]
        fn test_normalize_whitespace() {
            let mut api_pokemon = TEMPLATE_API_POKEMON.clone();
            api_pokemon.flavor_text_entries[0].language.name = "en".to_string();
            api_pokemon.flavor_text_entries[0].flavor_text =
                "description\nwith\u{c}funky whitespace".to_string();
            let pokemon: Pokemon = api_pokemon.into();
            assert_eq!(
                pokemon.description,
                Some("description with funky whitespace".to_string())
            );
        }
    }

    #[cfg(test)]
    mod integration_tests {

        use super::*;
        use std::fs;

        struct TestClient {}

        /// this acts as a fake server for our two external APIs
        /// each kind of response we want the APIs to get is
        /// associated with a unique string
        /// because the initial API request does not contain information
        /// that will be sent to the translation API
        /// you have to set this in the fake response from the pokeapi
        #[async_trait]
        impl Get for TestClient {
            async fn get(url: &str) -> Result<http::Response<isahc::AsyncBody>, isahc::Error> {
                if url.contains("bad_path") {
                    panic!("client should not of been called");
                } else if url.contains("pokeapi.co") {
                    let response = if url.contains("good_name") {
                        let f = File::open("./src/pokemon_example.json").await?;
                        let body = isahc::AsyncBody::from_reader(f);
                        http::Response::builder()
                            .status(200)
                            .header("Content-Type", "application/json")
                            .body(body)
                            .unwrap()
                    } else if url.contains("bad_name") {
                        let body = isahc::AsyncBody::empty();
                        http::Response::builder().status(404).body(body).unwrap()
                    } else if url.contains("rate_limit_translation_response") {
                        let data = fs::read_to_string("./src/pokemon_example.json").unwrap();
                        let mut pokemon = serde_json::from_str::<poke_api::Pokemon>(&data).unwrap();
                        pokemon.flavor_text_entries[0].flavor_text =
                            "rate_limit_translation_response".to_string();
                        let body = isahc::AsyncBody::from_bytes_static(
                            serde_json::to_vec(&pokemon).unwrap(),
                        );
                        http::Response::builder()
                            .status(200)
                            .header("Content-Type", "application/json")
                            .body(body)
                            .unwrap()
                    } else if url.contains("shakespeare") {
                        let data = fs::read_to_string("./src/pokemon_example.json").unwrap();
                        let mut pokemon = serde_json::from_str::<poke_api::Pokemon>(&data).unwrap();
                        pokemon.flavor_text_entries[0].flavor_text = "shakespeare".to_string();
                        let body = isahc::AsyncBody::from_bytes_static(
                            serde_json::to_vec(&pokemon).unwrap(),
                        );
                        http::Response::builder()
                            .status(200)
                            .header("Content-Type", "application/json")
                            .body(body)
                            .unwrap()
                    } else {
                        panic!("test case not covered: {}", url);
                    };
                    Ok(response)
                } else if url.contains("api.funtranslations.com") {
                    let response = if url.contains("shakespeare") {
                        let body = isahc::AsyncBody::from_bytes_static(
                            "{\"contents\": {\"translated\": \"shakespeare translation\"}}",
                        );
                        http::Response::builder()
                            .status(200)
                            .header("Content-Type", "application/json")
                            .body(body)
                            .unwrap()
                    } else if url.contains("rate_limit_translation_response") {
                        let body = isahc::AsyncBody::empty();
                        http::Response::builder().status(429).body(body).unwrap()
                    } else {
                        panic!("test case not covered: {}", url);
                    };
                    Ok(response)
                } else {
                    panic!("unexpected URL {}", url);
                }
            }
        }

        #[async_std::test]
        async fn test_bad_path() -> std::io::Result<()> {
            let app = make_server::<TestClient>();
            let req = tide::http::Request::new(
                Method::Get,
                Url::parse("https://example.com/bad_path").unwrap(),
            );
            let res: tide::Response = app.respond(req).await.unwrap();
            assert_eq!(res.status(), 404);
            Ok(())
        }
        #[async_std::test]
        async fn test_pokemon_bad_name() -> std::io::Result<()> {
            let app = make_server::<TestClient>();
            let req = tide::http::Request::new(
                Method::Get,
                Url::parse("https://example.com/pokemon/bad_name").unwrap(),
            );
            let res: tide::Response = app.respond(req).await.unwrap();
            assert_eq!(res.status(), 404);
            Ok(())
        }
        #[async_std::test]
        async fn test_pokemon_good_name() -> std::io::Result<()> {
            let app = make_server::<TestClient>();
            let req = tide::http::Request::new(
                Method::Get,
                Url::parse("https://example.com/pokemon/good_name").unwrap(),
            );
            let res: tide::Response = app.respond(req).await.unwrap();
            assert_eq!(res.status(), 200);
            Ok(())
        }
        #[async_std::test]
        async fn test_translate_bad_name() -> std::io::Result<()> {
            let app = make_server::<TestClient>();
            let req = tide::http::Request::new(
                Method::Get,
                Url::parse("https://example.com/pokemon/translated/bad_name").unwrap(),
            );
            let res: tide::Response = app.respond(req).await.unwrap();
            assert_eq!(res.status(), 404);
            Ok(())
        }
        #[async_std::test]
        async fn test_translate_shakespeare() -> std::io::Result<()> {
            let app = make_server::<TestClient>();
            let req = tide::http::Request::new(
                Method::Get,
                Url::parse("https://example.com/pokemon/translated/shakespeare").unwrap(),
            );
            let mut res: tide::Response = app.respond(req).await.unwrap();
            assert_eq!(res.status(), 200);
            let body = res.take_body().into_string().await.unwrap();
            let pokemon = serde_json::from_str::<Pokemon>(&body).unwrap();
            assert_eq!(
                pokemon.description,
                Some("shakespeare translation".to_string())
            );
            Ok(())
        }
        #[async_std::test]
        async fn test_translate_rate_limit() -> std::io::Result<()> {
            let app = make_server::<TestClient>();
            let req = tide::http::Request::new(
                Method::Get,
                Url::parse(
                    "https://example.com/pokemon/translated/rate_limit_translation_response",
                )
                .unwrap(),
            );
            let res: tide::Response = app.respond(req).await.unwrap();
            assert_eq!(res.status(), 200);

            Ok(())
        }
    }

    // ignore by default as it makes (many) real calls out to
    // and so will be slow and possibly flaky
    // but useful to be extra sure we work for all pokemon
    #[async_std::test]
    #[ignore]
    async fn test_end_to_end_all_pokemon() -> std::io::Result<()> {
        let app = make_server::<HttpClient>();
        let mut i = 1;
        loop {
            let mut pokeapi_res =
                isahc::get(&format!("https://pokeapi.co/api/v2/pokemon-species/{}", i)).unwrap();
            if pokeapi_res.status() == 404 {
                break;
            }
            let body_text = pokeapi_res.text().unwrap();
            let pokeapi_pokemon: serde_json::Value = serde_json::from_str(&body_text).unwrap();
            let correct_name = pokeapi_pokemon["name"].as_str().unwrap();
            let req = tide::http::Request::new(
                Method::Get,
                Url::parse(&format!("https://example.com/pokemon/{}", correct_name)).unwrap(),
            );
            let mut res: tide::Response = app.respond(req).await.unwrap();
            assert_eq!(res.status(), 200);
            let body = res.take_body().into_string().await.unwrap();
            let pokemon = serde_json::from_str::<Pokemon>(&body).unwrap();
            assert_eq!(pokemon.name, correct_name);
            // todo: assert one of the flavour texts matches the description
            i += 1;
        }

        Ok(())
    }

    // ignore by default as it uses the real translation API
    // which is rate limited to 5 req per minute
    // and 60 per hours
    // so too flaky to run often
    #[async_std::test]
    #[ignore]
    async fn test_end_to_end_translations() -> std::io::Result<()> {
        let app = make_server::<HttpClient>();

        let names = vec!["diglett", "bulbasaur"];
        let descriptions = vec!(
            // https://api.funtranslations.com/translate/yoda.json?text=Lives about one yard underground where it feeds on plant roots. It sometimes appears above ground.
            "On plant roots,  lives about one yard underground where it feeds.Above ground,  it sometimes appears.",
            // https://api.funtranslations.com/translate/shakespeare.json?text=A strange seed was planted on its back at birth. The plant sprouts and grows with this POKéMON.
            "A strange seed wast planted on its back at birth. The plant sprouts and grows with this pokémon.");
        for (i, name) in names.iter().enumerate() {
            let req = tide::http::Request::new(
                Method::Get,
                Url::parse(&format!("https://example.com/pokemon/translated/{}", name)).unwrap(),
            );
            let mut res: tide::Response = app.respond(req).await.unwrap();
            assert_eq!(res.status(), 200);
            let body = res.take_body().into_string().await.unwrap();
            let pokemon = serde_json::from_str::<Pokemon>(&body).unwrap();
            assert_eq!(&pokemon.name, name);
            assert_eq!(pokemon.description, Some(descriptions[i].to_string()));
        }
        Ok(())
    }
}

use isahc::AsyncReadResponseExt;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::{error::Error, fmt};

use crate::cache::Cache;

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
pub struct Pokemon {
    pub name: String,
    pub flavor_text_entries: Vec<FlavorText>,
    pub is_legendary: bool,
    pub habitat: Option<Habitat>,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
pub struct Habitat {
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
pub struct FlavorText {
    pub flavor_text: String,
    pub language: Language,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
pub struct Language {
    pub name: String,
}

#[derive(Debug)]
pub enum Errors {
    NotFound,
    Failed,
}

impl Error for Errors {}

impl fmt::Display for Errors {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Errors::NotFound => write!(f, "Pokemon not found"),
            Errors::Failed => write!(f, "Failed to get pokemon information from the API"),
        }
    }
}

static CACHE: Lazy<Cache<String, Pokemon>> = Lazy::new(Cache::new);

pub async fn lookup_pokemon<G: crate::Get>(name: &str) -> Result<Pokemon, Errors> {
    if let Some(pokemon) = CACHE.check(&name.to_string()).await {
        return Ok(pokemon);
    }
    let url = format!("https://pokeapi.co/api/v2/pokemon-species/{}", name);
    let mut res = G::get(&url).await.map_err(|_| Errors::Failed)?;
    match res.status() {
        http::StatusCode::NOT_FOUND => Err(Errors::NotFound),
        http::StatusCode::OK => {
            let a = res.text().await.unwrap();
            let pokemon = serde_json::from_str::<Pokemon>(&a).map_err(|_| Errors::Failed)?;
            CACHE.update(name.to_string(), &pokemon).await;
            Ok(pokemon)
        }
        _ => Err(Errors::Failed),
    }
}

#[cfg(test)]
mod test {

    use super::*;
    use std::fs::File;
    use std::io::BufReader;

    #[async_std::test]
    async fn deserialize_pokemon() -> std::io::Result<()> {
        let file = File::open("./src/pokemon_example.json").unwrap();
        let reader = BufReader::new(file);
        let expected_pokemon = Pokemon {
            name: "mewtwo".to_string(),
            flavor_text_entries: vec!(
                // manually delete the rest of the flavor texts
                // so that this wasn't massive
                FlavorText {
                    flavor_text: "It was created by\na scientist after\nyears of horrific\u{c}gene splicing and\nDNA engineering\nexperiments.".to_string(),
                    language: Language { name: "en".to_string() }
                },
                FlavorText {
                    flavor_text: "Its DNA is almost\nthe same as MEW's.\nHowever, its size\u{c}and disposition\nare vastly dif­\nferent.".to_string(),
                    language: Language { name: "en".to_string() }
                }),
                is_legendary: true,
                habitat: Some(Habitat { name: "rare".to_string() }
            )
        };
        let a = serde_json::from_reader::<_, Pokemon>(reader).unwrap();
        assert_eq!(expected_pokemon, a);
        Ok(())
    }
}

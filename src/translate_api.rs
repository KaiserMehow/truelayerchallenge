use form_urlencoded::byte_serialize;
use isahc::AsyncReadResponseExt;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::{error::Error, fmt};

use crate::cache::Cache;

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
struct Translated {
    contents: Contents,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
struct Contents {
    translated: String,
}

#[derive(Debug)]
pub enum Errors {
    RateLimit,
    Failed,
}

impl Error for Errors {}

impl fmt::Display for Errors {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Errors::RateLimit => write!(f, "We hit the external API rate limit"),
            Errors::Failed => write!(f, "Failed to get pokemon information from the API"),
        }
    }
}

static CACHE: Lazy<Cache<String, Translated>> = Lazy::new(Cache::new);

// todo: make kind an enum
pub async fn translate<G: crate::Get>(description: &str, kind: &str) -> Result<String, Errors> {
    let encoded_description: String = byte_serialize(description.as_bytes()).collect();
    let url = format!(
        "https://api.funtranslations.com/translate/{}.json?text={}",
        kind, encoded_description
    );
    if let Some(translated) = CACHE.check(&description.to_string()).await {
        return Ok(translated.contents.translated);
    }
    let mut res = G::get(&url).await.map_err(|_| Errors::Failed)?;
    match res.status() {
        http::StatusCode::TOO_MANY_REQUESTS => Err(Errors::RateLimit),
        http::StatusCode::OK => {
            // todo: can we avoid .text()
            let a = res.text().await.unwrap();
            let translated = serde_json::from_str::<Translated>(&a).map_err(|_| Errors::Failed)?;
            CACHE.update(description.to_string(), &translated).await;
            Ok(translated.contents.translated)
        }
        _ => Err(Errors::Failed),
    }
}

#[cfg(test)]
mod test {

    use super::*;
    use std::fs::File;
    use std::io::BufReader;

    #[async_std::test]
    async fn deserialize_translation() -> std::io::Result<()> {
        let file = File::open("./src/translated_example.json").unwrap();
        let reader = BufReader::new(file);
        let expected_translation = Translated {
            contents: Contents{
                translated: "Created by a scientist afteryears of horrific gene splicing and dna engineering experiments,  it was.".to_string()
            }
        };
        let a = serde_json::from_reader::<_, Translated>(reader).unwrap();
        assert_eq!(expected_translation, a);
        Ok(())
    }
}
